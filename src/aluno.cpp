#include <iostream>
#include "aluno.hpp"

Aluno::Aluno(){
  setNome("");
  setIdade("");
  setTelefone("");
  setMatricula("");
}

Aluno::Aluno(string nome, string idade, string telefone, int matricula){
  setNome(nome);
  setIdade(idade);
  setTelefone(telefone);
  setMatricula(matricula);
}

void Aluno::setMatricula(int matricula){
  this->matricula = matricula;
}
int Aluno::getMatricula(){
  return matricula;
}
void Aluno::setCreditos(int creditos){
  this->creditos = creditos;
}
int Aluno::getCreditos(){
  return creditos;
}
void Aluno::setSemestre(int semestre){
  this->semestre = semestre;
}
int Aluno::getSemestre(){
  return semestre;
}
void Aluno::setIra(float ira){
  this->ira = ira;
}
float Aluno::getIra(){
  return ira;
}
