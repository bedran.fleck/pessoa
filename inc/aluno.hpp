#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp"

class Aluno: public Pessoa{

private:
  int matricula;
  int creditos;
  int semestre;
  float ira;

public:
  Aluno();
  Aluno(string nome, string idade, string telefone, int telefone, int matricula);
  void setMatricula(int matricula);
  int getMatricula();
  void setCreditos(int creditos);
  int getCreditos();
  void setSemestre(int semestre);
  int getSemestre();
  void setIra(float ira);
  float getIra();

};

#endif
